# Contributions
SHAKTI is constantly growing and is always looking for active contributions from the community. The items listed are only some of the ideas that SHAKTI has on its current agenda.

If you have a new idea/contribution ()that is not listed below) please mail us at: shakti.iitm@gmail.com

## Verification

### Peripheral IPs
At SHAKTI we have developed quite a few open-source protocol IPs which are available here: https://gitlab.com/shaktiproject/uncore/devices .

Apart from above, several IPs are available in various languages: BSV, VHDL and Verilog from various sources like open-cores, free-cores, etc. which can be integrated into the SHAKTI environment.

Verification of these IPs is crucial and SHAKTI would like contributions from the community to help build a verification environment using UVM, assertions or even VIPs for the following IPs:

* USB 2.0 (Design at https://opencores.org/projects/usb)
* Ethernet (Design at https://opencores.org/projects/ethmac10g)
* SSD Card (https://opencores.org/projects/sdcard_mass_storage_controller)
* UART - SHAKTI
* I2C - SHAKTI
* QSPI - SHAKTI
* AXI4 - SHAKTI
* ONFI - SHAKTI
* SDRAM - SHAKTI
* DMA - SHAKTI

For contributions in the above fields please mail: shakti.iitm@gmail.com

### ISA Tests
SHAKTI offers 3 basic cores: e-class, c-class and i-class all supporting the RISC-V ISA. We are also actively developing Assembly level tests to check the compliance of these cores with the ISA-spec and also directed tests to check the sanity of the micro-architecture. 

For contributions in the above fields please mail: shakti.iitm@gmail.com

### Micro-Architecture Verification
The SHAKTI cores are built in a modular fashion using small but robust building blocks. These building blocks are used across designs and thus need
to be verified extensively. Some of these blocks include:
1. Caches and MMU engines
2. Coherency Protocols
3. Issue Queue for I-Class.
4. Branch predictors
5. Circular Buffer used in I-Class
6. Load/Store units
7. Register Alias Tables
8. Tree Based Priority Encoder

The verification of the above modules can be either in BSV with utilities like Bluecheck and OVL assertions. Verification at the verilog level would include developing UVM based frameworks for each of the modules.

For contributions in the above fields please mail: shakti.iitm@gmail.com

## Software

### Tools
* __RiTA__: This a python absed Riscv-Trace-Analyzer which can parse a trace-dump and provide statistics like register-usage, memory-histograms, branch statistics of an application post execution. For more information and contribution to RiTA please see: https://gitlab.com/shaktiproject/tools/rita
* __Tandem Verification__: This involves building a high-level framework which can enable quick/at-speed tandem verification of various RISC-V implementations like: custom-iss, spike, soft-shakti-cores, ASICs, FPGA based cores, formal-models etc. Such a framework can be used to qualify a design against a golden-design and quickly identify bugs. Please mail: shakti.iitm@gmail.com for more information.

### Device Drivers
Contributions are required to build device drivers for some of the SHAKTI IPs available here: https://gitlab.com/shaktiproject/uncore/devices for linux and also bare-metal frameworks like libfemto.

### OS Porting
SHAKTI cores can currently boot the Linux-OS and RTOSs like FreeRTOS and Zephyr. Ports for the following OSs will be entertained:
* Echronos
* seL4
* Debian/Fedora

## Design

### Devices
SHAKTI is currently looking for contributions in designing the following open-source IPs:

1. xSPI (Octal Spi)
2. RapidIO
3. GenZ
4. AXI4-Stream fabric
5. AHB Fabric

