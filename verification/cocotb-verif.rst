=========================================
CoCoTb Based Verification 
=========================================

.. sectnum::

.. contents:: Contents

Design Verification
~~~~~~~~~~~~~~~~~~~~~~~~~

 
.. figure:: verif_cclass.png
   :width: 350px


Why *NOT* SV-UVM
~~~~~~~~~~~~~~~~~~~~~~~~~~

CoCoTb with Verilator Setup
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
- https://antmicro.com/blog/2019/06/verilog-with-cocotb-and-verilator/
- https://cocotb.readthedocs.io/en/latest/