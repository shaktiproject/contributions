=========================================
RISC-V and Other Setups
=========================================

.. sectnum::

.. contents:: Contents

RISC-V Specifications
~~~~~~~~~~~~~~~~~~~~~~~~~

RISC-V is an open standard ISA adopted by SHAKTI for processor implementations. The specifications for the same are available as below. The two main specs to start with would be the user specification and the privileged specifications. 

`Spec Links <https://riscv.org/specifications>`_


RISC-V Tools Installation
~~~~~~~~~~~~~~~~~~~~~~~~~~
- Compiler installation takes a few hours. Follow the instructions in the `riscv-gnu-toolchain <https://github.com/riscv/riscv-gnu-toolchain>`_ repository.
- ISA Simulator known as spike can be installed from `riscv-isa-sim <https://github.com/riscv/riscv-tools>`_ repository along with other tools
- **Tip for any installation**:  Always create a build directory and use that path in the **--prefix** to control different installations of the same tool.

Git Usage
~~~~~~~~~~~~~~~~
If you are unfamiliar with Git, please follow the `basics <https://docs.gitlab.com/ee/gitlab-basics/>`_ to start with. `Shakti Project <https://gitlab.com/shaktiproject>`_ is maintained in GitLab and the follow for developers is given in the diagram below:

.. figure:: git_flow.png
   :width: 350px

- **master** branch maintains the stable most recently updated version
- All check-ins to the **master** branch is done using *merge requests* created through *GitLab issues*. 
- The branches created includes both the **bug-fixes** and new **features**
- On a merge, *GitLab CI/CD* is invoked to create a tagged release with the corresponding version number
- `SEMVER <https://semver.org/spec/v2.0.0.html>`_ versioning system in used in the CHANGELOG.md to maintain the repository version

GCC Installation
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash
  
  # Erase the current update-alternatives setup for gcc and g++:
  $ sudo update-alternatives --remove-all gcc 
  $ sudo update-alternatives --remove-all g++

  #Install Packages
  $ sudo apt-get install gcc-4.8 gcc-8.1 g++-4.8 g++-8.1

  # Symbolic links cc and c++ are installed by default.
  # We will install symbol links for gcc and g++, then link cc and c++ to gcc and g++ respectively.
  # (Note that the 10, 20 and 30 options are the priorities for each alternative.)

  $ sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.8 10
  $ sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8.1 20

  $ sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-4.8 10
  $ sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-8.1 20

  $ sudo update-alternatives --install /usr/bin/cc cc /usr/bin/gcc 30
  $ sudo update-alternatives --set cc /usr/bin/gcc

  $ sudo update-alternatives --install /usr/bin/c++ c++ /usr/bin/g++ 30
  $ sudo update-alternatives --set c++ /usr/bin/g++

  # Configure Alternatives
  # The last step is configuring the default commands for gcc, g++.
  # It's easy to switch between 4.8 and 8.1 interactively:

  $ sudo update-alternatives --config gcc
  $ sudo update-alternatives --config g++

Python Installation
~~~~~~~~~~~~~~~~~~~~~~~~~~
Install python version 3.6 and above. **ONLY** if you are facing issues with 3.6 installation, please follow the commands below. 

.. code-block:: bash

  # if facing any python issues
  $ sudo apt-get remove libpython3.6-minimal libpython3.6-stdlib python3.6 python3.6-minimal 
  $ sudo apt-get install python3.6 python3.6-dev
  
  $ sudo python3.6 -m pip install click xlwt gitpython pyyaml colorlog cerberus

